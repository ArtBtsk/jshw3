let userNumber = prompt("Введіть число:");

if (userNumber !== null && !isNaN(userNumber)) {
  const number = parseInt(userNumber, 10);

  const multiplesExist = [];

  if (number < 5) {
    console.log("Sorry, no numbers");
  } else {
    for (let i = 5; i <= number; i += 5) {
      multiplesExist.push(i);
    }

    console.log(...multiplesExist);
  }
} else {
  console.log("Введено некоректне число.");
}
